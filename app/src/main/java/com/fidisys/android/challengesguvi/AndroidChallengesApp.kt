package com.fidisys.android.challengesguvi

import android.app.Application
import com.fidisys.android.challengesguvi.log.TimberThreadDebugTree
import timber.log.Timber

class AndroidChallengesApp : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(TimberThreadDebugTree())
        }
    }
}