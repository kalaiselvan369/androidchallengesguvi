package com.fidisys.android.challengesguvi.retorfit

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class NetworkViewModel(application: Application) : AndroidViewModel(application) {

    private val _todos: MutableLiveData<List<Todo>> = MutableLiveData()
    val todos = _todos

    private var networkApi: SampleApi =
        InjectorUtils.provideSampleApi(application.applicationContext)

    fun getTodos() {
        viewModelScope.launch {
            val data = getTodosFromNetwork()
            Timber.d("todos %s", data)
            _todos.value = data
        }
    }

    private suspend fun getTodosFromNetwork(): List<Todo>? =
        withContext(Dispatchers.IO) {
            val response = networkApi.getTodos()
            return@withContext if (response.isSuccessful) response.body() else listOf<Todo>()
        }


}
