package com.fidisys.android.challengesguvi

object Constants {
    const val CONTENT_TYPE_KEY = "Content-Type"
    const val CONTENT_TYPE_VALUE = "application/json; charset=utf-8"
}