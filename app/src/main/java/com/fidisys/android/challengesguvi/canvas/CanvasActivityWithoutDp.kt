package com.fidisys.android.challengesguvi.canvas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.fidisys.android.challengesguvi.R

class CanvasActivityWithoutDp : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_canvas_without_dp)
    }
}
