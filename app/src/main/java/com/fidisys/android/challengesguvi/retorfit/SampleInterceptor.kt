package com.fidisys.android.challengesguvi.retorfit

import com.fidisys.android.challengesguvi.Constants
import okhttp3.Interceptor
import okhttp3.Response

class SampleInterceptor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val newRequest = chain.request().newBuilder()
            .header(Constants.CONTENT_TYPE_KEY, Constants.CONTENT_TYPE_VALUE)
            .build()
        return chain.proceed(newRequest)
    }
}