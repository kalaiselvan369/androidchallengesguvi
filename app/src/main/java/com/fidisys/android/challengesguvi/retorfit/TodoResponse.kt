package com.fidisys.android.challengesguvi.retorfit

data class TodoResponse(
    val todos : List<Todo>
)