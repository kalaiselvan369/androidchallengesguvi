package com.fidisys.android.challengesguvi.retorfit

import android.content.Context

object InjectorUtils {

    @Volatile
    private var networkRepository: NetworkRepository? = null

    private fun provideNetworkRepository(
        activityContext: Context,
        appContext: Context
    ): NetworkRepository {
        synchronized(this) {
            return networkRepository ?: networkRepository
            ?: createNetworkRepository(appContext)
        }
    }

    private fun createNetworkRepository(appContext: Context): NetworkRepository {
        return NetworkRepository(provideSampleApi(appContext))
    }

    fun provideSampleApi(appContext: Context): SampleApi {
        return SampleApi.create(appContext)
    }
}