package com.fidisys.android.challengesguvi.retorfit

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer

import com.fidisys.android.challengesguvi.R
import kotlinx.android.synthetic.main.retrofit_network.*
import timber.log.Timber

class NetworkFragment : Fragment() {

    private val viewModel by viewModels<NetworkViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.retrofit_network, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.todos.observe(viewLifecycleOwner, Observer {
            if (it != null && it.isNotEmpty()) {
                Timber.d("todo received %s",it)
                todo.text = it.get(0).title
            }
        })
        viewModel.getTodos()
    }

}
