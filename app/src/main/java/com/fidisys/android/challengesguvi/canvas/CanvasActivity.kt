package com.fidisys.android.challengesguvi.canvas

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.fidisys.android.challengesguvi.R

class CanvasActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_canvas)
    }
}
