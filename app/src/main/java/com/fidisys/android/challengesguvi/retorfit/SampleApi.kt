package com.fidisys.android.challengesguvi.retorfit

import android.content.Context
import com.fidisys.android.challengesguvi.BuildConfig
import com.fidisys.android.challengesguvi.R
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface SampleApi {

    @GET("/todos")
    suspend fun getTodos(): Response<List<Todo>>

    companion object Factory {
        fun create(appContext: Context): SampleApi {
            val retrofit = buildRetrofit(appContext)
            return retrofit.create(SampleApi::class.java)
        }

        // build retrofit
        private fun buildRetrofit(appContext: Context) = Retrofit.Builder()
            .baseUrl(appContext.getString(R.string.base_url))
            .client(sampleClient(appContext))
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        // build okhttp client
        private fun sampleClient(appContext: Context) = OkHttpClient().newBuilder()
            .addInterceptor(SampleInterceptor())
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else
                    HttpLoggingInterceptor.Level.BASIC
            })
            .build()
    }
}