package com.fidisys.android.challengesguvi.vector

import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.fidisys.android.challengesguvi.R
import kotlinx.android.synthetic.main.activity_vector_drawable.*

class VectorDrawable : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vector_drawable)
        image_location.setOnClickListener {
            changeColor(image_location)
        }
    }

    private fun changeColor(iv: ImageView) {
        DrawableCompat.setTint(
            iv.drawable,
            ContextCompat.getColor(applicationContext,
                R.color.colorAccent
            )
        )
    }
}
