package com.fidisys.android.challengesguvi.canvas

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import com.fidisys.android.challengesguvi.R
import timber.log.Timber

class CanvasView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val paint = Paint()
    private val textPaint = Paint()
    private val contentBgPaint = Paint()
    private val titleBgPaint = Paint()
    private var yMargin = 10f
    private var xMargin = 10f
    private var yPointer = 0f

    init {
        paint.color = ContextCompat.getColor(context, R.color.colorAccent)
        paint.isAntiAlias = true
        paint.style = Paint.Style.FILL_AND_STROKE

        textPaint.color = ContextCompat.getColor(context, R.color.colorAccent)
        textPaint.isAntiAlias = true
        textPaint.style = Paint.Style.FILL_AND_STROKE

        contentBgPaint.color = ContextCompat.getColor(context, R.color.colorPrimary)
        contentBgPaint.isAntiAlias = true
        contentBgPaint.style = Paint.Style.FILL_AND_STROKE

        titleBgPaint.color = ContextCompat.getColor(context, R.color.colorAccent)
        titleBgPaint.isAntiAlias = true
        titleBgPaint.style = Paint.Style.FILL_AND_STROKE

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        Timber.d("height %s", height.toString())
        Timber.d("width %s", width.toString())
        Timber.d("height dp %s", (height).toFloat().dpToPx())
        canvas?.apply {
            save()
            drawHeader(canvas)
            restore()
        }
    }

    private fun drawHeader(canvas: Canvas?, title: String = "Canvas") {
        //setPaintColor(contentBgPaint, getColor(R.color.colorPrimary))
        setTextSize(textPaint, 16f)
        setPaintColor(textPaint, getColor(R.color.colorPrimaryDark))
        setTypFace(textPaint, Typeface.create(Typeface.DEFAULT_BOLD, Typeface.BOLD))
        textAlignCentre(textPaint)
        yPointer = 60f
        val rect: RectF =
            RectF(
                xMargin.dpToPx(),
                yMargin.dpToPx(),
                width.toFloat() - xMargin.dpToPx(),
                yPointer.dpToPx()
            )

        canvas?.drawRect(rect, titleBgPaint)
        canvas?.drawText(title, 0, title.length, rect.centerX(), rect.centerY() + 15, textPaint)

        Timber.d("density %s", resources.displayMetrics.density)
        Timber.d("density dpi %s", resources.displayMetrics.densityDpi)
    }


    private fun getColor(colorId: Int) = ContextCompat.getColor(context, colorId)

    private fun setTextSize(paint: Paint, size: Float) {
        paint.textSize = size.spToPx()
    }

    private fun setPaintColor(paint: Paint, color: Int) {
        paint.color = color
    }

    private fun setTypFace(paint: Paint, typeface: Typeface) {
        paint.typeface = typeface
    }

    private fun textAlignCentre(paint: Paint) {
        paint.textAlign = Paint.Align.CENTER
    }

    private fun textAlignLeft(paint: Paint) {
        paint.textAlign = Paint.Align.LEFT
    }

    private fun textAlignRight(paint: Paint) {
        paint.textAlign = Paint.Align.RIGHT
    }

    private fun drawDivider(canvas: Canvas?, yPos: Float, xPos: Float = 0f) {
        canvas?.drawLine(xPos, yPos, width.toFloat(), yPos, textPaint)
    }


    private fun Float.dpToPx() = this * resources.displayMetrics.density

    private fun Int.dpToPx() = (this * resources.displayMetrics.density).toInt()

    private fun Float.spToPx() = this * resources.displayMetrics.scaledDensity

    private fun Int.pxToDp() = (this * 160) / resources.displayMetrics.densityDpi

    fun setBackgroundPaint(templateId: Int?) {
        when (templateId) {
            1 -> titleBgPaint.color = ContextCompat.getColor(context, R.color.colorAccent)
            2 -> titleBgPaint.color = ContextCompat.getColor(context, R.color.colorPrimary)
            3 -> titleBgPaint.color = ContextCompat.getColor(context, R.color.colorPrimaryDark)
            4 -> titleBgPaint.color = ContextCompat.getColor(context, R.color.colorAccent)
            else -> {
                titleBgPaint.color = ContextCompat.getColor(context, R.color.colorPrimaryDark)
            }
        }
    }
}

