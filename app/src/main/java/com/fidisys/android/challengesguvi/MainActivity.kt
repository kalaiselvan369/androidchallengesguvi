package com.fidisys.android.challengesguvi

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.fidisys.android.challengesguvi.canvas.CanvasActivity
import com.fidisys.android.challengesguvi.canvas.CanvasActivityWithoutDp
import com.fidisys.android.challengesguvi.retorfit.NetworkActivity
import com.fidisys.android.challengesguvi.vector.VectorDrawable
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button_vector_drawable.setOnClickListener {
            val intent = Intent(this, VectorDrawable::class.java)
            startActivity(intent)
        }

        button_retrofit_interceptor.setOnClickListener {
            val intent = Intent(this, NetworkActivity::class.java)
            startActivity(intent)
        }

        button_android_canvas.setOnClickListener {
            val intent = Intent(this, CanvasActivity::class.java)
            startActivity(intent)
        }

        button_android_canvas_nodp.setOnClickListener {
            val intent = Intent(this, CanvasActivityWithoutDp::class.java)
            startActivity(intent)
        }
    }
}
